using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Game : MonoBehaviour
{
    [SerializeField] private string mainMenuName;
    [SerializeField] private GameObject restartMenu;
    [SerializeField] private Text finalCoinCount;
    private int coinsCount;
    private int livesCount;

    private void Awake()
    {
        Game[] anotherGame = FindObjectsOfType<Game>();

        if (anotherGame.Length > 1)
        {
            Destroy(gameObject);
            Debug.Log("Destroy!");
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    public void RstartGame()
    {
        coinsCount = 0;
        SceneManager.LoadScene(mainMenuName);
    }

    private void GameOver()
    {
        Time.timeScale = 0f;
        restartMenu.SetActive(true);
    }

    public void LoseLive()
    {
        livesCount--;
        PlayerUI.ui.Setlives(livesCount);
        if (livesCount <= 0) GameOver();
    }

    public void AddCoins(int amount)
    {
        coinsCount += amount;
        PlayerUI.ui.ShowCoinCount(coinsCount);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
