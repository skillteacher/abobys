using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    public static PlayerUI ui;
    [SerializeField] private Text livesText;
    [SerializeField] private Text coinText;
    private int coinCount;

    private void Awake()
    {
        if (ui == null)
        {
            ui = this;
        }
        else
        {
            Destroy(this);
        }
    }

    public void Setlives(int amount)
    {
        livesText.text = amount.ToString();
    }

    public void ShowCoinCount(int amount)
    {
        coinText.text = amount.ToString();
    }
}
