using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Simple2D : MonoBehaviour
{
    [SerializeField] private string groundLayerName = "Ground";
    [SerializeField] private float speed = 5f;
    private Rigidbody2D enimyRigidbody;
    private bool isFacingRight;

    private void Awake()
    {
        enimyRigidbody = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()

    {
        Vector2 velocity = 
            new Vector2(isFacingRight ? speed : -speed, enimyRigidbody.velocity.y);

        enimyRigidbody.velocity = velocity;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer != LayerMask.NameToLayer(groundLayerName)) return;

        Flip();
    }

    private void Flip()
    {
        isFacingRight = !isFacingRight;
        transform.Rotate(0f, 180f, 0f);
    }
}
