using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class coin : MonoBehaviour
{
    [SerializeField] private string playerLayer = "player";
    private bool isCollected;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer != LayerMask.NameToLayer(playerLayer)) return;

        PickCoin();
    }

    private void PickCoin()
    {
        if (isCollected) return;
        isCollected = true;
        Game game = FindObjectOfType<Game>();
        if (game != null) game.AddCoins(1);
        else Debug.Log("�� ����� ������ Game");
        Destroy(gameObject);
    }
}
